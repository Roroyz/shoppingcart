import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Item } from '../model/Item';
import { CartItem } from '../model/CartItem';
import { Observable, of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';  

@Injectable()
export class ShoppingService {
  private Url = 'api/Items';
  public cartItems: CartItem[] = [];
  public GrandtotalPrice: number;
  public totalPrice: number;
  public totalQty: number;
  public totalItem: number;

  constructor(private http: HttpClient) {

  }

  getItems(): Observable<Item[]>{
    return this.http.get<Item[]>(this.Url);
  }

  getItem(id: number): Observable<Item> {
    return this.http.get<Item>(this.Url + "/" + id);
  }

   /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      //this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  addtoCart(i) {
    this.getItem(i.id);
    var count: number = 0;
    var ItemCountExist: number = 0;
    if (this.cartItems.length > 0) {
      for (count = 0; count < this.cartItems.length; count++) {
        if (this.cartItems[count].CItem_Name == i.name) {
          ItemCountExist = this.cartItems[count].CQty + 1;
          this.cartItems[count].CQty = ItemCountExist;
        }
      } 
    }
    if (ItemCountExist <= 0) {
      this.cartItems.push(
        new CartItem(i.id, i.name, i.image, i.description, i.addedBy, i.price, 1, i.price));

    }
    this.getItemTotalresult();
    alert(i.name + " added to cart!");
  }

  getItemTotalresult() {
    this.totalPrice = 0;
    this.totalQty = 0;
    this.GrandtotalPrice = 0;
    var count: number = 0;
    this.totalItem = this.cartItems.length;
    for (count = 0; count < this.cartItems.length; count++) {
      this.totalPrice += this.cartItems[count].CItem_Price;
      this.totalQty += (this.cartItems[count].CQty);
      this.GrandtotalPrice += this.cartItems[count].CItem_Price * this.cartItems[count].CQty;
    }
  }

  removefromCart(c) {
    alert("delete");  
    this.cartItems.splice(c, 1);
    this.getItemTotalresult();
  }
}
