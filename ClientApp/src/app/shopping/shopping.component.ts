import { Component, OnInit } from '@angular/core';
import { Item } from '../model/Item';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { CartItem } from '../model/CartItem';
import { ShoppingService } from '../service/shopping.service';

@Component({
    selector: 'app-shopping',
    templateUrl: './shopping.component.html',
    styleUrls: ['./shopping.component.css']
})
/** shopping component*/
export class ShoppingComponent implements OnInit {
/** shopping ctor */
  items: Item[] = [];
  item: Item;

  str: object[] = [{ msg: "lols" }, { msg: "lols" }, { msg: "asd" }, { msg: "abc" }, { msg: "123" }];
  
  constructor(private http: HttpClient, private shoppingService: ShoppingService) {
    
  }

  ngOnInit() {
    this.getItems();
  }

  getItems() {
    this.shoppingService.getItems().subscribe(data =>
    {
      this.items = data;
    });
  }

  getItem(id) {
    this.shoppingService.getItem(id).subscribe(data => {
      this.item = data;
    });
  }

  addtoCart(i) {
    this.shoppingService.addtoCart(i);
  }

}
