"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CartItem = /** @class */ (function () {
    function CartItem(CItem_ID, CItem_Name, CImage_Name, CDescription, CAddedBy, CItem_Price, CQty, CTotalPrice) {
        this.CItem_ID = CItem_ID;
        this.CItem_Name = CItem_Name;
        this.CImage_Name = CImage_Name;
        this.CDescription = CDescription;
        this.CAddedBy = CAddedBy;
        this.CItem_Price = CItem_Price;
        this.CQty = CQty;
        this.CTotalPrice = CTotalPrice;
    }
    return CartItem;
}());
exports.CartItem = CartItem;
//# sourceMappingURL=CartItem.js.map