using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoppingCart.Models;

namespace ShoppingCart.Data
{
    public class DbInitializer
    {
        public static void Initialize(ItemContext context)
        {
            context.Database.EnsureCreated();
            // Look for any items.
            if (context.Item.Any())
            {
                return;   // DB has been seeded
            }
            var items = new Item[]
            {
           new Item{Name="Axe", Price=250, Image="axe.jpg", Description="Made with titanium", AddedBy="JR"},
           new Item{Name="Anchor", Price=5000, Image="anchor.jpg", Description="for ships", AddedBy="JR"},
           new Item{Name="Balloons", Price=250, Image="balloons.jpg", Description="for parties", AddedBy="JR"},
           new Item{Name="Basket Ball", Price=1000, Image="basketball.jpg", Description="official nba basketball", AddedBy="JR"},
           new Item{Name="Bicycle", Price=10000, Image="bicycle.jpg", Description="mountain bike", AddedBy="JR"},
           new Item{Name="carpet", Price=60000, Image="carpet.jpg", Description="for dogs or houses /sq. ft.", AddedBy="JR"},
           new Item{Name="Chessboard", Price=3500, Image="chessboard.jpg", Description="made of oak wood", AddedBy="JR"},
           new Item{Name="Confetti", Price=200, Image="confetti.jpg", Description="for parties", AddedBy="JR"},
           new Item{Name="Clock", Price=700, Image="clock.jpg", Description="antique clock", AddedBy="JR"},
           new Item{Name="Cowboy Hat", Price=2500, Image="cowboyhat.jpg", Description="made with italian leather", AddedBy="JR"},
           new Item{Name="Door", Price=1500, Image="door.jpg", Description="made out of oak wood", AddedBy="JR"},
           new Item{Name="Dynamite", Price=2500, Image="dynamite.jpg", Description="plastic explosive", AddedBy="JR"},
           new Item{Name="Dolphin toy", Price=2000, Image="dolphintoy.jpg", Description="big and soft", AddedBy="JR"},
           new Item{Name="Earphones", Price=1000, Image="earphones.jpg", Description="noise cancelling", AddedBy="JR"},
           new Item{Name="Eggs", Price=150, Image="eggs.jpg", Description="1 tray of eggs", AddedBy="JR"},
           new Item{Name="Eraser", Price=50, Image="eraser.jpg", Description="colorful box of erasers", AddedBy="JR"},
           new Item{Name="flute", Price=50000, Image="flute.jpg", Description="made with titanium alloy", AddedBy="JR"},
           new Item{Name="french fries", Price=200, Image="frenchfries.jpg", Description="400g of fries", AddedBy="JR"},
           new Item{Name="fire extinguisher", Price=1250, Image="fireextinguisher.jpg", Description="3 lbs.", AddedBy="JR"},
           new Item{Name="Gel", Price=50, Image="gel.jpg", Description="benchfix hair gel", AddedBy="JR"},
           new Item{Name="Guitar", Price=5000, Image="guitar.jpg", Description="acoustic made with premium wood", AddedBy="JR"},
           new Item{Name="Golf club", Price=25000, Image="golfclub.jpg", Description="made with premium steel", AddedBy="JR"},
           new Item{Name="Hamburger", Price=30, Image="hamburger.jpg", Description="made with bread", AddedBy="JR"},
           new Item{Name="Handkerchief", Price=100, Image="handkerchief.jpg", Description="armando caruso handkerchiefs", AddedBy="JR"},
           new Item{Name="harp", Price=50000, Image="harp.jpg", Description="lever harp", AddedBy="JR"},
           new Item{Name="headband", Price=300, Image="headband.jpg", Description="helps you run faster", AddedBy="JR"},
           new Item{Name="ice cube tray", Price=200, Image="icecubetray.jpg", Description="made with premium plastic", AddedBy="JR"},
           new Item{Name="iron pickaxe", Price=700, Image="ironpickaxe.jpg", Description="wooden handle", AddedBy="JR"},
           new Item{Name="infant stuff toy", Price=200, Image="infantstufftoy.jpg", Description="comes with many colors", AddedBy="JR"},
           new Item{Name="jacket", Price=500, Image="jacket.jpg", Description="made with premium italian leather", AddedBy="JR"},
           new Item{Name="jump rope", Price=200, Image="jumprope.jpg", Description="lasts a lifetime", AddedBy="JR"},
           new Item{Name="jigsaw puzzle", Price=800, Image="jigsawpuzzle.jpg", Description="picture of a scenery", AddedBy="JR"},
           new Item{Name="kazoo", Price=200, Image="kazoo.jpg", Description="made with premium italian plastic", AddedBy="JK"},
           new Item{Name="keyboard", Price=9000, Image="keyboard.jpg", Description="SteelSeries Apex Pro", AddedBy="JC"},
           new Item{Name="kendo stick", Price=125000, Image="kendostick.jpg", Description="kendo stick set", AddedBy="JK"},
           new Item{Name="lamb sauce", Price=250, Image="lambsauce.jpg", Description="made by Gordon Ramsey /100g", AddedBy="JC"},
           new Item{Name="lamp", Price=500, Image="lamp.jpg", Description="table lamp", AddedBy="JR"},
           new Item{Name="leash", Price=200, Image="leash.jpg", Description="dog leash made with premium italian leather", AddedBy="JR"},
           new Item{Name="magnifying glass", Price=200, Image="magnifyingglass.jpg", Description="made with premium italian plastic", AddedBy="JK"},
           new Item{Name="milk tea", Price=70, Image="milktea.jpg", Description="mocha flavor", AddedBy="JC"},
           new Item{Name="motorcycle", Price=68000, Image="motorcycle.jpg", Description="Yamaha Mio Sporty", AddedBy="JR"},
           new Item{Name="neon night light", Price=300, Image="neonnightlight.jpg", Description="decorative piece", AddedBy="JK"},
           new Item{Name="noodles", Price=50, Image="noodles.jpg", Description="Nissis cup noodles", AddedBy="JC"},
           new Item{Name="nintendo switch", Price=20000, Image="nintendoswitch.jpg", Description="with preloaded games", AddedBy="JR"},
           new Item{Name="oatmeal", Price=250, Image="oatmeal.jpg", Description="3+1 bundle 400g", AddedBy="JK"},
           new Item{Name="olive oil", Price=200, Image="oliveoil.jpg", Description="250ml for cooking", AddedBy="JC"},
           new Item{Name="orange juice", Price=420, Image="orangejuice.jpg", Description="tang 24pcs 25g", AddedBy="JR"},
           new Item{Name="pail", Price=180, Image="pail.jpg", Description="made with premium italian plastic", AddedBy="JC"},
           new Item{Name="pizza", Price=200, Image="pizza.jpg", Description="Albertos Pizza supreme", AddedBy="JK"},
           new Item{Name="peanut butter", Price=320, Image="peanutbutter.jpg", Description="Jif 454g", AddedBy="JC"},
           new Item{Name="piano", Price=6000, Image="piano.jpg", Description="casio CTK-2550 portable piano", AddedBy="JK"},
           new Item{Name="quail eggs", Price=150, Image="quaileggs.jpg", Description="1 dozen", AddedBy="JK"},
           new Item{Name="queen minnie", Price=1000, Image="queenminnie.jpg", Description="premium stuff toy", AddedBy="JC"},
           new Item{Name="question mark pillow", Price=500, Image="questionmarkpillow.jpg", Description="made with premium italian leather", AddedBy="JR"},
           new Item{Name="rabbit", Price=2000, Image="rabbit.jpg", Description="baby rabbits fresh from the womb", AddedBy="JR"},
           new Item{Name="rain coat", Price=200, Image="raincoat.jpg", Description="made with organic plastic", AddedBy="JC"},
           new Item{Name="ribbon", Price=100, Image="ribbon.jpg", Description="25 yards silk ribbon", AddedBy="JC"},
           new Item{Name="shirt", Price=200, Image="shirt.jpg", Description="custom made cutton shirt", AddedBy="JC"},
           new Item{Name="slippers", Price=350, Image="slippers.jpg", Description="Islander slippers", AddedBy="JC"},
           new Item{Name="shoes", Price=500000, Image="shoes.jpg", Description="air jordan 12 ovo", AddedBy="JC"},
           new Item{Name="tea set", Price=1000, Image="teaset.jpg", Description="made with chinese glass", AddedBy="JC"},
           new Item{Name="teddy bear", Price=1300, Image="teddybear.jpg", Description="human size teddy bear", AddedBy="JC"},
           new Item{Name="toy train", Price=150, Image="toytrain.jpg", Description="toy train set for kids", AddedBy="JC"},
           new Item{Name="underwear", Price=250, Image="underwear.jpg", Description="mens boxers 6 pcs.", AddedBy="JC"},
           new Item{Name="ukulele", Price=1500, Image="ukulele.jpg", Description="made with premium oak wood", AddedBy="JC"},
           new Item{Name="umbrella", Price=200, Image="umbrella.jpg", Description="generic 7-eleven umbrella", AddedBy="JC"},
           new Item{Name="vase", Price=150, Image="vase.jpg", Description="made with glass", AddedBy="JC"},
           new Item{Name="vest", Price=1000, Image="vest.jpg", Description="motorcycle vest", AddedBy="JC"},
           new Item{Name="vinegar", Price=70, Image="vinegar.jpg", Description="apple cider vinegar", AddedBy="JC"},
           new Item{Name="wallet", Price=100, Image="wallet.jpg", Description="long wallet for women", AddedBy="JC"},
           new Item{Name="wheel", Price=300, Image="wheel.jpg", Description="for generic cars", AddedBy="JC"},
           new Item{Name="watch", Price=4000, Image="watch.jpg", Description="Casio G-Shock Men's Digital Black Resin Strap Watch DW-5600BB-1DR", AddedBy="JC"},
           new Item{Name="Xaomi phone", Price=10000, Image="xaomiphone.jpg", Description="Redmi note 7", AddedBy="JC"},
           new Item{Name="xylophone", Price=1100, Image="xylophone.jpg", Description="medium size, made with aluminum foil", AddedBy="JC"},
           new Item{Name="yacht", Price=500000, Image="yacht.jpg", Description="Standard yacht", AddedBy="JC"},
           new Item{Name="yam", Price=880, Image="yam.jpg", Description="rice flour", AddedBy="JC"},
           new Item{Name="yo-yo", Price=4000, Image="yo-yo.jpg", Description="premium quality", AddedBy="JC"},
           new Item{Name="zipper", Price=200, Image="zipper.jpg", Description="50pcs assorted colors", AddedBy="JC"},
           new Item{Name="zebra helmet", Price=2000, Image="zebrahelmet.jpg", Description="for motorcycle enthusiast", AddedBy="JC"},


            };
            foreach (Item i in items)
            {
                context.Item.Add(i);
            }
            context.SaveChanges();
        }
    }
}
